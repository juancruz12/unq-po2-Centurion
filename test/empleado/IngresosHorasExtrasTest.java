package empleado;
import org.junit.jupiter.api.BeforeEach;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class IngresosHorasExtrasTest {
	private IngresosHorasExtras ingresoPlus;
	private IngresosHorasExtras ingresoExtra;
	@BeforeEach
	public void setUp() {
		ingresoPlus=new IngresosHorasExtras("MAYO","lamparitaRota",30,8);
		ingresoExtra=new IngresosHorasExtras("Junio","cortePelo",40,10);
	}
	@Test
	public void testConstructor() {
		assertEquals("MAYO",ingresoPlus.getMes());
		assertEquals("lamparitaRota",ingresoPlus.getConcepto());
		assertEquals(8,ingresoPlus.cantHorasExtras());
	}

}
