package empleado;
import org.junit.jupiter.api.BeforeEach;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import supermercado.Producto;

class EmpleadoTest {
	private Empleado fabian;
	private Ingreso ingresoAbr;
	private Ingreso ingresoFeb;
	private IngresosHorasExtras ingresoPlus;
	@BeforeEach
	public void setUp() {
		fabian = new Empleado();
		ingresoAbr= new Ingreso("Abril","laburo",859);
		ingresoFeb= new Ingreso("Febrero","laburoSucio",41);
		ingresoPlus= new IngresosHorasExtras("Marzo","Vigilancia",1000,45);
	}
	@Test
	public void testGetTotalPercibido() {
		assertEquals(0,fabian.getTotalPercibido());
		fabian.agregarIngreso(ingresoAbr);
		fabian.agregarIngreso(ingresoFeb);
		fabian.agregarIngreso(ingresoPlus);
		assertEquals(1900,fabian.getTotalPercibido());
	}
	@Test
	public void testGetMontoImponible() {
		fabian.agregarIngreso(ingresoAbr);
		fabian.agregarIngreso(ingresoFeb);
		fabian.agregarIngreso(ingresoPlus);
		assertEquals(900,fabian.getMontoImponible());
	}
	@Test
	public void testImpuestoAPagar() {
		fabian.agregarIngreso(ingresoAbr);
		fabian.agregarIngreso(ingresoFeb);
		fabian.agregarIngreso(ingresoPlus);
		assertEquals(18,fabian.getImpuestoAPagar());
	}

}
