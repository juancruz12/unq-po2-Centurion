package empleado;
import org.junit.jupiter.api.BeforeEach;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class IngresoTest {
	private Ingreso ingresoA;
	private Ingreso ingresoB;

	@BeforeEach
	public void setUp() {
		ingresoA=new Ingreso("Julio","limpiaPiso",100);
		ingresoB=new Ingreso("Diciembre","baņoAlPerro",800);
	}
	@Test
	public void testConstructor() {
		assertEquals("Julio",ingresoA.getMes());
		assertEquals("baņoAlPerro",ingresoB.getConcepto());
		assertEquals(800,ingresoB.getMonto());
	}
}
