package superCaja;
import mercadoCaja.ProductoSuper;
import org.junit.jupiter.api.BeforeEach;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class ProductoSuperTest {
	
	private ProductoSuper galletita;
	private ProductoSuper gaseosa;
	@BeforeEach
	void setUp() {
		galletita=new ProductoSuper(10,15.25,false);
		gaseosa=new ProductoSuper(20,20,true);
	}
	@Test
	public void constructorTest() {
		assertEquals(15.25,galletita.getPrecio());
		assertEquals(18,gaseosa.getPrecio());
	}
}
