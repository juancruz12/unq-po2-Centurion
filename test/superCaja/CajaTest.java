package superCaja;
import java.util.ArrayList;
import mercadoCaja.CosasAPagar;
import mercadoCaja.Caja;
import mercadoCaja.ProductoSuper;
import org.junit.jupiter.api.BeforeEach;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class CajaTest {
	private Caja cajaChino;
	private ProductoSuper gaseosa;
	private ProductoSuper arroz;
	private ProductoSuper galletita;
	private ArrayList<CosasAPagar> listaCosas;
	
	@BeforeEach
	void setUp() {
		cajaChino=new Caja();
		galletita=new ProductoSuper(10,15.25,false);
		gaseosa=new ProductoSuper(20,20,true);
		arroz=new ProductoSuper(20,2,false);
	}
	@Test 
	public void precioTotalAPagarTest() {
		listaCosas.add(gaseosa);
		listaCosas.add(galletita);
		listaCosas.add(arroz);
		assertEquals(35.25,cajaChino.precioTotalAPagar(listaCosas));
		assertEquals(19,arroz.getStock());
	}

}
