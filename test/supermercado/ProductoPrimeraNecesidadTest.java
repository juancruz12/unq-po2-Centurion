package supermercado;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ProductoPrimeraNecesidadTest {

	private ProductoPrimeraNecesidad leche;
	private ProductoPrimeraNecesidad manteca;
	
	@BeforeEach
	public void setUp() {
		leche = new ProductoPrimeraNecesidad("Leche", 8d, false);
		manteca = new ProductoPrimeraNecesidad("manteca",10d,false);
	}
	
	@Test
	public void testCalcularPrecio() {
		assertEquals(new Double(7.2), leche.getPrecio(10));
	}
	@Test
	public void testCalcularPrecioConDescuentoAplicado() {
		assertEquals(new Double (9) ,manteca.getPrecio(10));
	}
}
