package unq;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import java.util.ArrayList;
public class CounterTestCase {
	
	private Counter counterPrueba;
	@Before
	public void campoPrueba() {
		counterPrueba=new Counter();
		counterPrueba.addNumber(1);
		counterPrueba.addNumber(3);
		counterPrueba.addNumber(5);
		counterPrueba.addNumber(7);
		counterPrueba.addNumber(9);
		counterPrueba.addNumber(1);
		counterPrueba.addNumber(1);
		counterPrueba.addNumber(1);
		counterPrueba.addNumber(1);
		counterPrueba.addNumber(4);
	}
	@Test
	public void prubaCantPares() {
		assertTrue(counterPrueba.cantPares()==1);
	}
	@Test
	public void pruebaCantImpares() {
		assertTrue(counterPrueba.cantImpares()==9);
	}
	@Test
	public void pruebaCantMultiplos() {
		assertTrue(counterPrueba.cantNumerosMultiplos(2)==5);
	}
}
