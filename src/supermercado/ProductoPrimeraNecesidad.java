package supermercado;

public class ProductoPrimeraNecesidad extends Producto {

	public ProductoPrimeraNecesidad(String nombreX, double precioX, boolean esPrecioC) {
		super(nombreX, precioX, esPrecioC);
		// TODO Auto-generated constructor stub
	}

	public double getPrecio(double descuento) {
		return this.precio - (this.precio * (descuento/100));
	}
}
