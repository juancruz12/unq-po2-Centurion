package supermercado;
import java.util.ArrayList;

public class Supermercado {
	private String nombre;
	private String direccion;
	private ArrayList<Producto> productos=new ArrayList<Producto>();
	
	public Supermercado(String nombreX,String direccionX) {
		nombre=nombreX;
		direccion=direccionX;
	}
	public String getNombre() {
		return nombre;
	}
	public String getDireccion() {
		return direccion;
	}
	public int getCantidadDeProductos() {
		return productos.size();
	}
	public double getPrecioTotal() {
		double contador=0;
		for (Producto producto:productos) {
			contador=contador+producto.getPrecio();
		}
		return contador;
	}
	public void agregarProducto(Producto productoX) {
		productos.add(productoX);
	}
}
