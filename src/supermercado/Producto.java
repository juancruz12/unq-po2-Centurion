package supermercado;

public class Producto {
	private String nombre;
	protected double precio;
	private boolean esPrecioCuidado=false;
	
	public Producto(String nombreX,double precioX,boolean esPrecioC) {
		this.nombre=nombreX;
		this.precio=precioX;
		this.esPrecioCuidado=esPrecioC;
	}
	public Producto(String nombreX,double precioX) {
		this.nombre=nombreX;
		this.precio=precioX;
	}
	
	public void aumentarPrecio(double aumentoX) {
		precio=precio+aumentoX;
	}
	public double getPrecio() {
		return precio;
	}
	public String getNombre(){
		return nombre;
	}
	public boolean esPrecioCuidado() {
		return esPrecioCuidado;
	}
}

