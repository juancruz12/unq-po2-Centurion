package model;

public class Point {
	private int x;
	private int y;
	
	public Point crearCoordPoint(int numberX,int numberY) {
		Point nuevoPunto=new Point();
		nuevoPunto.setX(numberX);
		nuevoPunto.setY(numberY);
		return nuevoPunto;
	}
	public void crearPuntoDefault() {
		Point nuevoPunto=new Point();
		nuevoPunto.setX(0);
		nuevoPunto.setY(0);
	}
	
	public int getX() {
		return x; 
	}
	public void setX(int numberX) {
		x=numberX; 
	}
	public int getY() {
		return y; 
	}
	public void setY(int numberY) {
		y=numberY; 
	}
	public void modificarCoord(int numberX,int numberY) {
		this.setX(numberX);
		this.setY(numberY);
	}
	/*nose si estan bien implementados los constructores*/
	public Point sumaDePoints(Point puntoA) {
		 int nuevoX=x+puntoA.getX();
		 int nuevoY=y+puntoA.getY();
		 return this.crearCoordPoint(nuevoX, nuevoY);
	}	
}
