package unq;
import java.util.ArrayList;

public class MultiOperador {
	
	public int sumaArrayList(ArrayList<Integer> arrayX) {
		int contador=0;
		for (int numero:arrayX) {
			contador=contador+numero;
		}
		return contador;
	}
	/*Consulta con el array , nose si esta bien el remove()*/
	public int restarArrayList(ArrayList<Integer> arrayX) {
		int contador=arrayX.get(0);
		arrayX.remove(0);
		for (int numero:arrayX) {
			contador=contador-numero;
		}
		return contador;
	}
	public int multiplicarArrayList(ArrayList<Integer> arrayX) {
		int contador=1;
		for (int numero:arrayX) {
			contador=contador*numero;
		}
		return contador;
	}
	
}
