package unq;
import java.util.ArrayList;

public class Counter{
	private ArrayList<Integer> arrayNumeros=new ArrayList<Integer>();
	
	public ArrayList<Integer> getArray() {
		return arrayNumeros;
	}
	public void addNumber(int number) {
		arrayNumeros.add(number);
	}
	public int cantPares() {
		int contador=0;
		for (int numero:arrayNumeros) {
			if(numero%2==0) {
				contador=contador+1;
			}
		}
		return contador;		
	}
	public int cantImpares() {
		int contador=0;
		for (int numero:arrayNumeros) {
			if(numero%2 != 0) {
				contador=contador+1;
			}
		}
		return contador;		
	}
	public int cantNumerosMultiplos(int numeroX) {
		int contador=0;
		for (int numero:arrayNumeros) {
			if (numeroX%numero==0) {
				contador=contador+1;
			}
		}
		return contador;
	}
	
}

