package email;

public interface IServidor extends ServidorBasico {

	public float tazaDeTransferencia();

	public void resetear();
	
	public void realizarBackUp();
}
