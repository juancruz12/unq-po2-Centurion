package email;

public class TransferenciaCorreos {
	private ServidorPop servidor;
	private ClienteEMail emailCliente;
	
	public void recibirNuevos() {
		servidor.recibirNuevos(emailCliente.nombreUsuario(),emailCliente.passusuario());
	}
	public void enviarCorreo(String asunto, String destinatario, String cuerpo) {
		Correo correo = new Correo(asunto, destinatario, cuerpo);
		servidor.enviar(correo);
	}
	public void conectar() {
		servidor.conectar(emailCliente.nombreUsuario(),emailCliente.passusuario());
	}
}
