package empleado;

public class IngresosHorasExtras extends Ingreso {
	private float cantHoras;
	public IngresosHorasExtras(String mesX, String conceptoX, float montoX,float horasX) {
		super(mesX, conceptoX, montoX);
		cantHoras=horasX;
	}
	public float cantHorasExtras() {
		return cantHoras;
	}
	@Override
	public float montoImponible() {
		return 0;
	}
}
