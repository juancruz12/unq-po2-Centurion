package empleado;
import java.util.ArrayList;
public class Ingreso {
	private String mes;
	private String concepto;
	private float monto;
	
	public Ingreso(String mesX,String conceptoX,float montoX) {
		mes=mesX;
		concepto=conceptoX;
		monto=montoX;
	}
	
	public String getMes() {
		return mes;
	}
	public String getConcepto() {
		return concepto;
	}
	public float getMonto() {
		return monto;
	}
	public float montoImponible() {
		return monto;
	}
}
