package empleado;

import java.util.ArrayList;

public class Empleado {
	private ArrayList<Ingreso> ingresos = new ArrayList<Ingreso>();
	
	public float getTotalPercibido() {
		float contador=0;
		for(Ingreso ingreso:ingresos) {
			contador =contador + ingreso.getMonto();
		}
		return contador;
	}
	public float getMontoImponible() {
		float contador=0;
		for (Ingreso ingreso:ingresos) {
			contador=contador+ingreso.montoImponible();
		}
		return contador; 
	}
	public float getImpuestoAPagar() {
		return (2*this.getMontoImponible())/100;
	}
	public void agregarIngreso(Ingreso ingresoX) {
		ingresos.add(ingresoX);
	}
}
