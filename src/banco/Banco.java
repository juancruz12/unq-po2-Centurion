package banco;
import java.util.ArrayList;
public class Banco {
	private ArrayList<Cliente> clientes;
	private ArrayList<SolicitudDeCredito> solicitudes;
	
	public void agregarCliente(Cliente clienteX) {
		clientes.add(clienteX);
	}
	public void agregarSolicitud(SolicitudDeCredito solicitudX) {
		solicitudes.add(solicitudX);
	}
	public void verificarSolicitudes(Cliente clienteX,SolicitudDeCredito creditoX) {
		for (SolicitudDeCredito solicitud:solicitudes) {
			if (solicitud.esAceptable()) {
				solicitud.cliente().recibirCredito();
			}
		}
	}
	public float montoTotalDesembolso() {
		float valorTotal=0;
		for (SolicitudDeCredito solicitud:solicitudes) { //EXISTE FOR CONDICIONAL?
			if (solicitud.esAceptable()) {
				valorTotal=valorTotal+solicitud.montoSolicitado();
			}
		}
		return valorTotal;
	}
}
