package banco;

public interface SolicitudDeCredito {
	public int plazoMeses();
	public float montoSolicitado();
	public Cliente cliente() ;
	public float montoCuota();
	public Boolean esAceptable();
}
