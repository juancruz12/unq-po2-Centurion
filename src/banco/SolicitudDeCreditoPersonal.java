package banco;

public class SolicitudDeCreditoPersonal implements SolicitudDeCredito {
	private Cliente clienteSolicitante;
	private float montoSolicitado;;
	private int plazoMeses;
	
	public  SolicitudDeCreditoPersonal(Cliente clienteX,int monto,int plazoMes) {
		clienteSolicitante=clienteX;
		montoSolicitado=monto;
		plazoMeses=plazoMes;
	}
	
	public Cliente cliente() {
		return clienteSolicitante;
	}
	public float montoSolicitado() {
		return montoSolicitado;
	}
	public int plazoMeses() {
		return plazoMeses;
	}
	public float montoCuota() {
		return montoSolicitado/plazoMeses;
	}
	public Boolean esAceptable() {
		return clienteSolicitante.sueldoNetoAnio()>15000 &&
					this.montoCuota()< (70*clienteSolicitante.sueldoNetoMes()/100);
	}
}
