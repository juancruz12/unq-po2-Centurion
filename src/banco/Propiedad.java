package banco;

public class Propiedad {
	private String descripcion;
	private String direccion;
	private float valorFiscal;
	
	public String getDescripcion() {
		return descripcion;
	}
	public String getDireccion() {
		return direccion;
	}
	public float getValorFiscal() {
		return valorFiscal;
	}
}
