package banco;
import java.util.ArrayList;
public class SolicitudDeCreditoHipotecario implements SolicitudDeCredito {
	private Cliente clienteSolicitante;
	private float montoSolicitado;
	private int plazoMeses;
	private ArrayList<Propiedad> garantiaPropiedades;
	
	public SolicitudDeCreditoHipotecario(Cliente clienteX,float monto,int plazoMes,ArrayList<Propiedad> propiedades) {
		clienteSolicitante=clienteX;
		montoSolicitado=monto;
		plazoMeses=plazoMes;
		garantiaPropiedades = propiedades;
	}
	
	public float montoSolicitado() {
		return montoSolicitado;
	}
	public int plazoMeses() {
		return plazoMeses;
	}
	public Cliente cliente() {
		return clienteSolicitante;
	}
	public float montoCuota() {
		return montoSolicitado/plazoMeses;
	}
	public float valorFiscalTotal() {
		float valor=0;
		for (Propiedad propiedad:garantiaPropiedades) {
			valor=valor+propiedad.getValorFiscal();
		}
		return valor;
	}
	public Boolean esAceptable() {
		return this.montoCuota()<(50*clienteSolicitante.sueldoNetoMes()/100) &&
				montoSolicitado<70*this.valorFiscalTotal()/100;
	}
}
