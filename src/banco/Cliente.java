package banco;
import java.util.ArrayList;
public class Cliente {
	private String nombre;
	private String apellido;
	private String direccion;
	private int edad;
	private float sueldoNetoMes;
	private float sueldoNetoAnio;
	private SolicitudDeCredito solicitud;
	private ArrayList<Propiedad> propiedades;
	
	public String getDireccion() {
		return direccion;
	}
	public String getNombre() {
		return nombre;
	}
	public String getApellido() {
		return apellido;
	}
	public int getEdad() {
		return edad;
	}
	public float sueldoNetoMes() {
		return sueldoNetoMes;
	}
	public float sueldoNetoAnio() {
		return sueldoNetoAnio;
	}
	public void recibirCredito() {
		solicitud = null;
	}
	public void generarSolicitud() {
		//CONSULTA COMO GENERAR LA SOLICITUD, genera la solicitud y la agrega a la lista de solicitudes del banco
		//crear otra clase que conecte clase cliente con clase banco y agregue en la lista de solicitudes , la nueva solicitud
	}
}
