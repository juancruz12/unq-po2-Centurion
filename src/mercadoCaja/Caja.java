package mercadoCaja;
import java.util.ArrayList;

public class Caja {
	
	/*public void agregarProductoACompra(CosasAPagar cosaX) {
		productosYFacturas.add(cosaX);
	}*/
	public double precioTotalAPagar(ArrayList<CosasAPagar> listaX ) {
		double total=0;
		for (CosasAPagar cosa:listaX) {
			total=total+cosa.getPrecio();
			cosa.pagar();
		}
		return total;
	}
}
