package mercadoCaja;

abstract class Factura implements CosasAPagar {
	private Agencia agencia;
	public void pagar() {
		agencia.registrarPago(this);
	}
}
