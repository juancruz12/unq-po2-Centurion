package mercadoCaja;

public class ProductoSuper implements CosasAPagar {
	private int stock;
	private double precio;
	private Boolean esProductoCoop;
	
	public ProductoSuper(int stockX,double precioX,Boolean esProductoCoopX) {
		stock=stockX;
		precio=precioX;
		esProductoCoop=esProductoCoopX;
	}
	public double getPrecio() {
		if (esProductoCoop) {
			return precio - (10*precio)/100;
		}
		else {return precio;}
	}
	public void reducirStock() {
		stock=stock-1;
	}
	public void pagar() {
		this.reducirStock();
	}
	public int getStock() {
		return stock;
	}
}
