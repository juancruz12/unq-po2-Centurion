package mercadoCaja;

public interface CosasAPagar {
	public double getPrecio();
	public void reducirStock();
	public void pagar();
}
